//package com.example.proevent.utils;
//
//import java.io.ByteArrayOutputStream;
//import java.util.zip.Deflater;
//import java.util.zip.Inflater;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//public class ImageUtils {
//    private static final Logger log = LoggerFactory.getLogger(ImageUtils.class);
//
//    public static byte[] compressImage(byte[] data) {
//        Deflater deflater = new Deflater();
//        deflater.setLevel(Deflater.BEST_COMPRESSION);
//        deflater.setInput(data);
//        deflater.finish();
//
//        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length)) {
//            byte[] tmp = new byte[4*1024];
//            while (!deflater.finished()) {
//                int size = deflater.deflate(tmp);
//                outputStream.write(tmp, 0, size);
//            }
//            return outputStream.toByteArray();
//        } catch (Exception e) {
//            log.error("Erreur lors de la compression de l'image", e);
//            return data;
//        } finally {
//            deflater.end();
//        }
//    }
//
//    public static byte[] decompressImage(byte[] data) {
//        Inflater inflater = new Inflater();
//        inflater.setInput(data);
//        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length)) {
//            byte[] tmp = new byte[4*1024];
//            while (!inflater.finished()) {
//                int count = inflater.inflate(tmp);
//                outputStream.write(tmp, 0, count);
//            }
//            return outputStream.toByteArray();
//        } catch (Exception e) {
//            log.error("Erreur lors de la décompression de l'image", e);
//            return data;
//        } finally {
//            inflater.end();
//        }
//    }
//}