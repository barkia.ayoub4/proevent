package com.example.proevent.utils;
public class FileStorageUtil {

    private static final String BASE_URL = "http://localhost:8080/";

    public static String constructFullImageUrl(String fileName, String folderName) {
        return BASE_URL + folderName + "/" + fileName;
    }
}

