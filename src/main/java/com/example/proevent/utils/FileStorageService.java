package com.example.proevent.utils;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
@Service("fileStorageService")
public class FileStorageService {
    @Value("${file.upload-dir}")
    private String uploadDir;

    private final ResourceLoader resourceLoader;

    public FileStorageService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public String storeFile(MultipartFile file, String folderName)  throws IOException {
        String originalFileName = StringUtils.cleanPath(file.getOriginalFilename());
        String fileExtension = StringUtils.getFilenameExtension(originalFileName);
        String baseFileName = StringUtils.stripFilenameExtension(originalFileName);
        String fileName = baseFileName + "." + fileExtension;

        // Calculer le chemin de fichier absolu
        if(uploadDir.startsWith("classpath:")) {
            String classpathLocation=uploadDir.substring("classpath:".length());
            Resource resource =resourceLoader.getResource("classpath:"+ classpathLocation);
            if(resource.exists()){
                try {
                    uploadDir=resource.getURL().getPath();
                }
                catch (IOException e){
                }
            }
        }

        // Copier le fichier dans le répertoire cible
        Path targetLocation = Paths.get(uploadDir,folderName,fileName.replace("\\",File.separator));
        Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        return fileName;
    }
}
