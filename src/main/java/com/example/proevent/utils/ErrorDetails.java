package com.example.proevent.utils;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
public class ErrorDetails {
    LocalDateTime timestamp;
    String message;
    String details;
}

