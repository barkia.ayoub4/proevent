package com.example.proevent.Event.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class TypeEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String designation;

    @OneToMany(mappedBy = "typeEvent" )
    @JsonIgnore
    private Collection<Event> events;



    public void updateTypeEvent(TypeEvent newTypeEvent) {
        if (newTypeEvent.getDesignation() != null) {
            this.setDesignation(newTypeEvent.getDesignation());
        }
    }
}
