package com.example.proevent.Event.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Partenaire {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nom;

    private String prenom;

    private String description;
    private String profileImage;
    @ManyToMany(mappedBy = "partenaires")
    private Collection<Event> events;


    @Transient
    public String getPhotosImagePath() {
        if (profileImage == null || id == null)
            return null;
        return "/partenaire_photo/" + profileImage;
    }
    public void updatePartenaire(Partenaire newPartenaire) {
        if (newPartenaire.getNom() != null) {
            this.setNom(newPartenaire.getNom());
        }
        if (newPartenaire.getPrenom() != null) {
            this.setPrenom(newPartenaire.getPrenom());
        }
        if (newPartenaire.getDescription() != null) {
            this.setDescription(newPartenaire.getDescription());
        }
        if (newPartenaire.getProfileImage() != null) {
            this.setProfileImage(newPartenaire.getProfileImage());
        }
    }


}
