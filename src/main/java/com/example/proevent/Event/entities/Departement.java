package com.example.proevent.Event.entities;

import com.example.proevent.authentification.entities.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Departement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    private String nameDep;

    @OneToMany(mappedBy = "departement")
    @JsonIgnore
    private Collection<Event> events;

    @OneToMany(mappedBy = "departement")
    @JsonIgnore
    private Collection<User> users;



    public void updateDepartement(Departement newDepartement) {
        if (newDepartement.getNameDep() != null) {
            this.setNameDep(newDepartement.getNameDep());
        }
    }



}

