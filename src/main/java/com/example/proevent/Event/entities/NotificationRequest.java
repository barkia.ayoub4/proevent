package com.example.proevent.Event.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
public class NotificationRequest {
    private String title;
    private String body;
    private String topic;
    private String token;
}