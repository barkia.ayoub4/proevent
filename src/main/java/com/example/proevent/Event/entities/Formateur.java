package com.example.proevent.Event.entities;

import com.example.proevent.authentification.entities.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.HashSet;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Formateur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;
    private String name ;

    @Email
    @Column(unique = true)
    private  String email ;
    private String image;

    @JsonIgnore
    @OneToMany(mappedBy = "formateur")
    private Collection<Session> sessions;

    @JsonIgnore
    @OneToMany(mappedBy = "formateur")
    private Collection<Event> events;

//    @JsonIgnore
//    @OneToMany(mappedBy = "formateur", cascade = CascadeType.ALL)
//    private Collection<Event> events;

    public void updateFormateur(Formateur newForamteur){
        if (newForamteur.getName() != null) {
            this.setName(newForamteur.getName());
        }
        if (newForamteur.getEmail() != null) {
            this.setEmail(newForamteur.getEmail());
        }
    }


}
