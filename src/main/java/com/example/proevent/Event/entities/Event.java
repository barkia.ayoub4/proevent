package com.example.proevent.Event.entities;

import com.example.proevent.authentification.entities.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Collection;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "event")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private String lieu;
    private String linkLieu;
    private String date;
    private String startTime;
    private String endTime;
    private String image;

    @JsonIgnore
    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<UserEvent> users;


    @ManyToOne
    @JoinColumn(name = "departement_id")
    private Departement departement;

    @ManyToOne
    private TypeEvent typeEvent;

    @ManyToOne()
    @JoinColumn(name = "formateur_id")
    private Formateur formateur;


    @ManyToMany
    private Collection<Partenaire> partenaires;

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
    private Collection<Session> sessions;




    public void updateEvent(Event newEvent){
        if (newEvent.getLieu() != null) {
            this.setLieu(newEvent.getLieu());
        }
        if (newEvent.getDate() != null) {
            this.setDate(newEvent.getDate());
        }
        if (newEvent.getStartTime() != null) {
            this.setStartTime(newEvent.getStartTime());
        }

    }


}
