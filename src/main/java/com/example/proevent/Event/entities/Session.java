package com.example.proevent.Event.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Collection;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String startTime;

    private String endTime;

    private String description;

    @JsonIgnore
    @ManyToOne
    private Event event;

    @ManyToOne()
    @JoinColumn(name = "formateur_id")
    private Formateur formateur;


    public void updateSession(Session newSession) {
        if (newSession.getTitle() != null) {
            this.setTitle(newSession.getTitle());
        }
        if (newSession.getStartTime() != null) {
            this.setStartTime(newSession.getStartTime());
        }
        if (newSession.getEndTime() != null) {
            this.setEndTime(newSession.getEndTime());
        }
        if (newSession.getDescription() != null) {
            this.setDescription(newSession.getDescription());
        }
    }

}
