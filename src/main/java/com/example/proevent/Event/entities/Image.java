//package com.example.proevent.Event.entities;
//
//import com.example.proevent.authentification.entities.User;
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import jakarta.persistence.*;
//import lombok.*;
//@Entity
//@Getter
//@Setter
//@NoArgsConstructor
//@AllArgsConstructor
//@ToString(exclude = "user")  // Exclure 'user' pour éviter la récursivité
//@Builder
//public class Image {
//    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
//    private Long id;
//    private String name;
//    private String description;
//    private String type;
//    private String imagePath;
//
//    private User user;
//
//}