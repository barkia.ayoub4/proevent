package com.example.proevent.Event.repositories;

import com.example.proevent.Event.entities.UserEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserEventRepository extends JpaRepository<UserEvent, Long> {
    Optional<UserEvent> findByUserIdAndEventId(Integer userId, Long eventId);
    List<UserEvent> findByEventId(Long eventId);
    List<UserEvent> findByUserId(Long userId);

    @Transactional
    void deleteByEventIdAndUserId(Long eventId, Long userId);


//    @Query("SELECT e.event.id, " +
//            "SUM(CASE WHEN ue.rate < '3' THEN 1 ELSE 0 END) as unhappyCount, " +
//            "SUM(CASE WHEN ue.rate = '3' THEN 1 ELSE 0 END) as satisfiedCount, " +
//            "SUM(CASE WHEN ue.rate > '3' THEN 1 ELSE 0 END) as happyCount " +
//            "FROM UserEvent ue JOIN ue.event e " +
//            "GROUP BY e.id")
//    List<Object[]> findRatingCountsForEachEvent();

}
