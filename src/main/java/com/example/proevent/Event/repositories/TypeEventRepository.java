package com.example.proevent.Event.repositories;

import com.example.proevent.Event.entities.TypeEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TypeEventRepository extends JpaRepository<TypeEvent,Integer> {
    TypeEvent findByDesignation(String designation);

    @Query("SELECT te.designation, COUNT(e.id) FROM TypeEvent te " +
            "JOIN te.events e GROUP BY te.designation")
    List<Object[]> countEventsByType();


}
