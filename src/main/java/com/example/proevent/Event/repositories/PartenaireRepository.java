package com.example.proevent.Event.repositories;

import com.example.proevent.Event.entities.Partenaire;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PartenaireRepository extends JpaRepository<Partenaire,Long> {
}
