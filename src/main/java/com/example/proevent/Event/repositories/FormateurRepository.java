package com.example.proevent.Event.repositories;

import com.example.proevent.Event.entities.Formateur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FormateurRepository extends JpaRepository<Formateur,Long> {
}
