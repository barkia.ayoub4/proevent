package com.example.proevent.Event.repositories;

import com.example.proevent.Event.entities.Departement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface DepartementRepository extends JpaRepository<Departement,Integer> {
    void deleteById(Integer departementId);
    boolean existsById(Integer id);


    @Query("SELECT d.nameDep, COUNT(e) FROM Departement d JOIN d.events e GROUP BY d.nameDep")
    List<Object[]> countEventsByDepartment();

}
