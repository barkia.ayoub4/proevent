package com.example.proevent.Event.repositories;

import com.example.proevent.Event.entities.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EventRepository extends JpaRepository<Event,Long> {
    @Query("SELECT COUNT(e) FROM event e")
    long countAllEvents();
}
