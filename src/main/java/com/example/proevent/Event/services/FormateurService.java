package com.example.proevent.Event.services;

import com.example.proevent.Event.entities.Event;
import com.example.proevent.Event.entities.Formateur;
import com.example.proevent.Event.entities.Session;
import com.example.proevent.Event.repositories.EventRepository;
import com.example.proevent.Event.repositories.FormateurRepository;
import com.example.proevent.Event.repositories.SessionRepository;
import com.example.proevent.utils.FileStorageService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.util.List;

@Service
public class FormateurService {
    private final FormateurRepository formateurRepository;
    private final FileStorageService fileStorageService;
    private final SessionRepository sessionRepository;
    private final EventRepository eventRepository;


    public FormateurService(FormateurRepository formateurRepository, FileStorageService fileStorageService, SessionRepository sessionRepository, SessionRepository sessionRepository1, EventRepository eventRepository) {
        this.formateurRepository = formateurRepository;
        this.fileStorageService = fileStorageService;
        this.sessionRepository = sessionRepository1;
        this.eventRepository = eventRepository;
    }

    public List<Formateur> getAllFormateurs() {
        return formateurRepository.findAll();
    }

    public Formateur getFormateurById(Long formateurId) {
        return formateurRepository.findById(formateurId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Formateur not found"));
    }

    public Formateur saveFormateur(Formateur formateur, MultipartFile file  ) throws IOException {
        String folderName = "formateur";
        String fileName = fileStorageService.storeFile(file, folderName);
        String imageUrl ="http://localhost:8080/formateur/" + fileName ;
        formateur.setImage(imageUrl);
        return formateurRepository.save(formateur);
    }

    public Formateur updateFormateur(Formateur newFormateur, Long id) {
        if (!formateurRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Formateur not found");
        }
        Formateur existingFormateur = formateurRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Formateur not found"));
        existingFormateur.updateFormateur(newFormateur);
        return formateurRepository.save(existingFormateur);
    }

    public void deleteFormateur(Long formateurId) {
        Formateur formateur = formateurRepository.findById(formateurId)
                .orElseThrow(() -> new RuntimeException("Formateur non trouvé avec l'ID " ));
        for (Session session : formateur.getSessions()) {
            session.setFormateur(null);
            sessionRepository.save(session);
        }

        for (Event event : formateur.getEvents()) {
            event.setFormateur(null);
            eventRepository.save(event);
        }

        formateurRepository.delete(formateur);
    }
}
