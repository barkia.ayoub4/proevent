package com.example.proevent.Event.services;

import com.example.proevent.Event.entities.Event;
import com.example.proevent.Event.repositories.EventRepository;
import com.example.proevent.utils.FileStorageService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.util.List;

@Service

public class EventService {
    private final EventRepository eventRepository;
    private final FileStorageService fileStorageService;


    public EventService(EventRepository eventRepository, FileStorageService fileStorageService) {
        this.eventRepository = eventRepository;
        this.fileStorageService = fileStorageService;
    }


    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    public Event getEventById(Long eventId) {
        return eventRepository.findById(eventId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found"));
    }

    public Event saveEvent(Event event, MultipartFile file) throws IOException {
        String folderName = "event";
        String fileName = fileStorageService.storeFile(file, folderName);
      //  Event savedEvent= eventRepository.save(event);
        String imageUrl ="http://localhost:8080/event/" + fileName ;
        event.setImage(imageUrl);
        return eventRepository.save(event);
    }

    public Event update(Event newEvent, Long id) {
        if (!eventRepository.existsById(id)) {throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found");}
        Event existingEvent= eventRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found"));
        existingEvent.updateEvent(newEvent);
        return eventRepository.save(existingEvent);
    }

    public void deleteEvent(Long eventId) {
        eventRepository.deleteById(eventId);
    }


    public long getTotalNumberOfEvents() {
        return eventRepository.countAllEvents();
    }
}
