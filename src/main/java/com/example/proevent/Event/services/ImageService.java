//package com.example.proevent.Event.services;
//
//import com.example.proevent.Event.entities.Image;
//import com.example.proevent.Event.repositories.ImageRepository;
//import com.example.proevent.utils.ImageUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.File;
//import java.io.IOException;
//import java.nio.file.Files;
//import java.util.Optional;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//@Service
//public class ImageService {
//    private static final Logger log = LoggerFactory.getLogger(ImageService.class);
//
//    @Autowired
//    private ImageRepository imageRepository;
//    final String uploadDir = "C:\\Users\\AYOUB\\Desktop\\pfeback\\proback\\src\\main\\resources\\images\\";
//
//    final String baseUrl = "http://10.0.2.2:8080/images/";
//    public ImageService() {
//        File directory = new File(uploadDir);
//        if (!directory.exists()) {
//            boolean mkdirs = directory.mkdirs();
//            if (mkdirs) {
//                log.info("Répertoire créé: " + directory.getAbsolutePath());
//            } else {
//                log.error("Échec de la création du répertoire: " + directory.getAbsolutePath());
//            }
//        }
//    }
//    public String uploadImageToFileSystem(MultipartFile file) throws IOException {
//        String fileName = file.getOriginalFilename();
//        String filePath = uploadDir + fileName;
//        String accessiblePath = baseUrl + fileName;
//
//
//        Image fileData=imageRepository.save(Image.builder()
//                .name(file.getOriginalFilename())
//                .type(file.getContentType())
//                .imagePath(accessiblePath)
//                .build());
//
//        file.transferTo(new File(filePath));
//        if (fileData != null) {
//            return "file uploaded successfully : " + filePath;
//        }
//        return null;
//    }
//
////    public byte[] downloadImageFromFileSystem(String fileName) throws IOException {
////        Optional<Image> fileData = imageRepository.findByName(fileName);
////        String filePath=fileData.get().getImagePath();
////        byte[] images = Files.readAllBytes(new File(filePath).toPath());
////        return images;
////    }
//
//
//    /* public String uploadImage(MultipartFile file) throws IOException {
//        log.info("Début de l'upload de l'image");
//        byte[] compressedImage = ImageUtils.compressImage(file.getBytes());
//        Image imageData = imageRepository.save(Image.builder()
//                .name(file.getOriginalFilename())
//                .type(file.getContentType())
//                .image(compressedImage).build());
//        if (imageData != null) {
//            log.info("Image uploadée avec succès : " + file.getOriginalFilename());
//            return "Image uploadée avec succès : " + file.getOriginalFilename();
//        }
//        log.error("Échec de l'upload de l'image");
//        return null;
//    }
//
//    public byte[] downloadImage(String fileName){
//        log.info("Début du téléchargement de l'image");
//        Optional<Image> dbImageData = imageRepository.findByName(fileName);
//        if (dbImageData.isPresent()) {
//            byte[] decompressedImage = ImageUtils.decompressImage(dbImageData.get().getImage());
//            log.info("Image téléchargée avec succès");
//            return decompressedImage;
//        } else {
//            log.error("Image non trouvée avec le nom : " + fileName);
//            return null;
//        }
//    }*/
//
//
///*
//    public String saveImage(MultipartFile file, String name, String description) throws IOException {
//        Image image = new Image();
//        image.setName(name);
//        image.setDescription(description);
//        image.setImage(file.getBytes());
//        imageRepository.save(image);
//        return "Image saved in DB";
//    } */
//
//
//}
