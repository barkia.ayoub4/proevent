package com.example.proevent.Event.services;

import com.example.proevent.Event.entities.Event;
import com.example.proevent.Event.entities.TypeEvent;
import com.example.proevent.Event.repositories.EventRepository;
import com.example.proevent.Event.repositories.TypeEventRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TypeEventService {
    private final TypeEventRepository typeEventRepository;
    private final EventRepository eventRepository;

    public TypeEventService(TypeEventRepository typeEventRepository, EventRepository eventRepository) {
        this.typeEventRepository = typeEventRepository;
        this.eventRepository = eventRepository;
    }

    public List<TypeEvent> getAllTypeEvents() {
        return typeEventRepository.findAll();
    }

    public TypeEvent getTypeEventById(Integer typeEventId) {
        return typeEventRepository.findById(typeEventId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "TypeEvent not found"));
    }

    public TypeEvent saveTypeEvent(TypeEvent typeEvent) {

        return typeEventRepository.save(typeEvent);
    }

    public TypeEvent updateTypeEvent(TypeEvent newTypeEvent, Integer id) {
        if (!typeEventRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "TypeEvent not found");
        }
        TypeEvent existingTypeEvent = typeEventRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "TypeEvent not found"));
        existingTypeEvent.updateTypeEvent(newTypeEvent);
        return typeEventRepository.save(existingTypeEvent);
    }

    public void deleteTypeEvent(Integer typeEventId) {
        TypeEvent type = typeEventRepository.findById(typeEventId).orElseThrow(() -> new RuntimeException("Type d'événement non trouvé"));
        for (Event event : type.getEvents()) {
            event.setTypeEvent(null);
            eventRepository.save(event);
        }
        typeEventRepository.deleteById(typeEventId);
    }

    public Map<String, Long> getEventCountByType() {
        List<Object[]> results = typeEventRepository.countEventsByType();
        Map<String, Long> eventCounts = new HashMap<>();
        for (Object[] result : results) {
            eventCounts.put((String) result[0], (Long) result[1]);
        }
        return eventCounts;
    }

}
