package com.example.proevent.Event.services;

import com.example.proevent.Event.entities.Partenaire;
import com.example.proevent.Event.repositories.PartenaireRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

import java.util.List;

@Service
public class PartenaireService {
    private final PartenaireRepository partenaireRepository;

    public PartenaireService(PartenaireRepository partenaireRepository) {
        this.partenaireRepository = partenaireRepository;
    }

    public List<Partenaire> getAllPartenaires() {
        return partenaireRepository.findAll();
    }

    public Partenaire getPartenaireById(Long partenaireId) {
        return partenaireRepository.findById(partenaireId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Partenaire not found"));
    }

    public Partenaire savePartenaire(Partenaire partenaire) {
        return partenaireRepository.save(partenaire);
    }

    public Partenaire updatePartenaire(Partenaire newPartenaire, Long id) {
        if (!partenaireRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Partenaire not found");
        }
        Partenaire existingPartenaire = partenaireRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Partenaire not found"));
        existingPartenaire.setNom(newPartenaire.getNom());
        existingPartenaire.setPrenom(newPartenaire.getPrenom());
        existingPartenaire.setDescription(newPartenaire.getDescription());
        return partenaireRepository.save(existingPartenaire);
    }

    public void deletePartenaire(Long partenaireId) {
        partenaireRepository.deleteById(partenaireId);
    }
}
