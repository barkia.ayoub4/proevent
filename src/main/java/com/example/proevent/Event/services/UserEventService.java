package com.example.proevent.Event.services;

import com.example.proevent.Event.entities.UserEvent;
import com.example.proevent.Event.repositories.UserEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UserEventService {

    private final UserEventRepository userEventRepository;

    @Autowired
    public UserEventService(UserEventRepository userEventRepository) {
        this.userEventRepository = userEventRepository;
    }

    public UserEvent saveUserEvent(UserEvent userEvent) {
        return userEventRepository.save(userEvent);
    }

    public List<UserEvent> getAllUserEvents() {
        return userEventRepository.findAll();
    }

    public Optional<UserEvent> getUserEventById(Long id) {
        return userEventRepository.findById(id);
    }

    public void deleteUserEvent(Long id) {
        userEventRepository.deleteById(id);
    }

    public UserEvent updateUserEventRate(Integer userId, Long eventId, String rate) {
        Optional<UserEvent> userEventOptional = userEventRepository.findByUserIdAndEventId(userId, eventId);
        if (userEventOptional.isPresent()) {
            UserEvent userEvent = userEventOptional.get();
            userEvent.setRate(rate);
            return userEventRepository.save(userEvent);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "UserEvent not found");
        }
    }

    public List<UserEvent> getUserEventsByEventId(Long eventId) {
        return userEventRepository.findByEventId(eventId);
    }


    public UserEvent updateUserEvent(UserEvent newUserEvent) {
        if (newUserEvent == null) {
            throw new IllegalArgumentException("Provided UserEvent object is null");
        }
        return userEventRepository.save(newUserEvent);
    }

    public UserEvent saveUserEventIfNotExists(UserEvent newUserEvent) {
        Optional<UserEvent> existingUserEvent = userEventRepository.findByUserIdAndEventId(newUserEvent.getUser().getId(), newUserEvent.getEvent().getId());
        if (existingUserEvent.isPresent()) {
            throw new IllegalStateException("UserEvent already exists for this user and event");
        }
        if (newUserEvent.getRate() == null) {
            newUserEvent.setRate(null);
        }
        return userEventRepository.save(newUserEvent);
    }



    public Map<String, Integer> countUserSatisfactionByEventId(Long eventId) {
        List<UserEvent> userEvents = userEventRepository.findByEventId(eventId);
        Map<String, Integer> satisfactionCount = new HashMap<>();
        satisfactionCount.put("Not Satisfied", 0);
        satisfactionCount.put("Satisfied", 0);
        satisfactionCount.put("Super Satisfied", 0);

        for (UserEvent userEvent : userEvents) {
            String rateString = userEvent.getRate();
            if (rateString != null) {
                int rate = Integer.parseInt(rateString);
                if (rate == 1 || rate == 2) {
                    satisfactionCount.put("Not Satisfied", satisfactionCount.get("Not Satisfied") + 1);
                } else if (rate == 3) {
                    satisfactionCount.put("Satisfied", satisfactionCount.get("Satisfied") + 1);
                } else if (rate == 4 || rate == 5) {
                    satisfactionCount.put("Super Satisfied", satisfactionCount.get("Super Satisfied") + 1);
                }
            }
        }
        return satisfactionCount;
    }





}
