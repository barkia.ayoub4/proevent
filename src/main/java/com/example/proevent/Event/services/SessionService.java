package com.example.proevent.Event.services;

import com.example.proevent.Event.entities.Formateur;
import com.example.proevent.Event.entities.Session;
import com.example.proevent.Event.repositories.FormateurRepository;
import com.example.proevent.Event.repositories.SessionRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

import java.util.List;

@Service
public class SessionService {
    private final SessionRepository sessionRepository;
    private final FormateurRepository formateurRepository;

    public SessionService(SessionRepository sessionRepository, FormateurRepository formateurRepository) {
        this.sessionRepository = sessionRepository;
        this.formateurRepository = formateurRepository;
    }

    public List<Session> getAllSessions() {
        return sessionRepository.findAll();
    }

    public Session getSessionById(Long sessionId) {
        return sessionRepository.findById(sessionId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Session not found"));
    }

    public Session saveSession(Session session) {
        return sessionRepository.save(session);
    }

    public Session updateSession(
            Session newSession,
            Long sessionId
            ) {
        if (!sessionRepository.existsById(sessionId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Session not found");
        }
        Session existingSession = sessionRepository.findById(sessionId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Session not found"));

        // Update session attributes here
        existingSession.setTitle(newSession.getTitle());
        existingSession.setStartTime(newSession.getStartTime());
        existingSession.setEndTime(newSession.getEndTime());
        existingSession.setDescription(newSession.getDescription());

        return sessionRepository.save(existingSession);
    }

    public void deleteSession(Long sessionId) {
        Session session = sessionRepository.findById(sessionId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Session not found"));
        sessionRepository.delete(session);  // Supprime la session
    }}
