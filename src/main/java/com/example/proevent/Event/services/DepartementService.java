package com.example.proevent.Event.services;

import com.example.proevent.Event.entities.Departement;
import com.example.proevent.Event.repositories.DepartementRepository;
import com.example.proevent.authentification.entities.User;
import com.example.proevent.authentification.repositries.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class DepartementService {
    private final DepartementRepository departementRepository;
    private final UserRepository userRepository;

    public DepartementService(DepartementRepository departementRepository, UserRepository userRepository) {
        this.departementRepository = departementRepository;
        this.userRepository = userRepository;
    }

    public List<Departement> getAllDepartements() {
        return departementRepository.findAll();
    }

    public Optional<Departement> getDepartementById(Integer departementId) {
        return departementRepository.findById(departementId);
    }

    public Departement saveDepartement(Departement departement) {
        return departementRepository.save(departement);
    }

    public Departement updateDepartement(Departement newDepartement, Integer id) {
        if (!departementRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Departement not found");
        }
        Departement existingDepartement = departementRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Departement not found"));
        existingDepartement.updateDepartement(newDepartement);
        return departementRepository.save(existingDepartement);
    }

    public void deleteDepartement(Integer departementId) {

        Departement departement = departementRepository.findById(departementId).orElseThrow(() -> new RuntimeException("Département non trouvé"));
        for (User user : departement.getUsers()) {
            user.setDepartement(null);
            userRepository.save(user);
        }

        departementRepository.deleteById(departementId);
    }

    public Map<String, Integer> getEventCountByDepartment() {
        List<Object[]> results = departementRepository.countEventsByDepartment();
        Map<String, Integer> eventCountByDepartment = new LinkedHashMap<>();
        for (Object[] result : results) {
            String departmentName = (String) result[0];
            Integer count = ((Number) result[1]).intValue();
            eventCountByDepartment.put(departmentName, count);
        }
        return eventCountByDepartment;
    }


}
