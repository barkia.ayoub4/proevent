package com.example.proevent.Event.controllers;

import com.example.proevent.Event.entities.TypeEvent;
import com.example.proevent.Event.services.TypeEventService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/type-events")
public class TypeEventController {

    private final TypeEventService typeEventService;

    public TypeEventController(TypeEventService typeEventService) {
        this.typeEventService = typeEventService;
    }

    @Operation(summary = "Get list of all event types")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of event types retrieved successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = TypeEvent.class)) })
    })
    @GetMapping()
    public List<TypeEvent> typeEventList() {
        return typeEventService.getAllTypeEvents();
    }

    @Operation(summary = "Get an event type by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Event type retrieved successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = TypeEvent.class)) }),
            @ApiResponse(responseCode = "404", description = "Event type not found", content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<TypeEvent> getTypeEventById(@PathVariable Integer id) {
        TypeEvent typeEvent = typeEventService.getTypeEventById(id);
        return new ResponseEntity<>(typeEvent, HttpStatus.OK);
    }

    @Operation(summary = "Create a new event type")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Event type created successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = TypeEvent.class)) })
    })
    @PostMapping
    public ResponseEntity<TypeEvent> createTypeEvent(@RequestBody TypeEvent typeEvent) {
        TypeEvent createdTypeEvent = typeEventService.saveTypeEvent(typeEvent);
        return new ResponseEntity<>(createdTypeEvent, HttpStatus.CREATED);
    }

    @Operation(summary = "Update an event type")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Event type updated successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = TypeEvent.class)) }),
            @ApiResponse(responseCode = "404", description = "Event type not found", content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity<TypeEvent> update(@Valid @RequestBody TypeEvent newTypeEvent, @PathVariable Integer id) {
        TypeEvent updatedTypeEvent = typeEventService.updateTypeEvent(newTypeEvent, id);
        return new ResponseEntity<>(updatedTypeEvent, HttpStatus.OK);
    }

    @Operation(summary = "Delete an event type")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Event type deleted successfully", content = @Content),
            @ApiResponse(responseCode = "404", description = "Event type not found", content = @Content)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTypeEvent(@PathVariable Integer id) {
        typeEventService.deleteTypeEvent(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Get event stats by type")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Event stats retrieved successfully",
                    content = { @Content(mediaType = "application/json") })
    })
    @GetMapping("/stats")
    public ResponseEntity<Map<String, Long>> getEventStats() {
        Map<String, Long> stats = typeEventService.getEventCountByType();
        return new ResponseEntity<>(stats, HttpStatus.OK);
    }
}
