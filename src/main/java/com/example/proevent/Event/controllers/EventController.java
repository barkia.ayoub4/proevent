package com.example.proevent.Event.controllers;

import com.example.proevent.Event.entities.Departement;
import com.example.proevent.Event.entities.Event;
import com.example.proevent.Event.entities.Formateur;
import com.example.proevent.Event.entities.TypeEvent;
import com.example.proevent.Event.repositories.DepartementRepository;
import com.example.proevent.Event.repositories.EventRepository;
import com.example.proevent.Event.repositories.FormateurRepository;
import com.example.proevent.Event.repositories.TypeEventRepository;
import com.example.proevent.Event.services.EventService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/events")
public class EventController {

    private final EventService eventService;
    private final EventRepository eventRepository;
    private final DepartementRepository departementRepository;
    private final TypeEventRepository typeEventRepository;
    private final FormateurRepository formateurRepository;

    public EventController(EventService eventService, EventRepository eventRepository,
                           DepartementRepository departementRepository,
                           TypeEventRepository typeEventRepository,
                           FormateurRepository formateurRepository) {
        this.eventService = eventService;
        this.eventRepository = eventRepository;
        this.departementRepository = departementRepository;
        this.typeEventRepository = typeEventRepository;
        this.formateurRepository = formateurRepository;
    }

    @Operation(summary = "Get all events")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of all events",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Event.class)) })
    })
    @GetMapping()
    public List<Event> EventList(){
        return eventService.getAllEvents();
    }

    @Operation(summary = "Get an event by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Event found",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Event.class)) }),
            @ApiResponse(responseCode = "404", description = "Event not found",
                    content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<Event> getEventById(@PathVariable Long id) {
        Event event = eventService.getEventById(id);
        return new ResponseEntity<>(event, HttpStatus.OK);
    }

    @Operation(summary = "Create a new event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Event created successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Event.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid input",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity<Event> createEvent(
            @RequestParam("title") String title,
            @RequestParam("description") String description,
            @RequestParam("lieu") String lieu,
            @RequestParam("linkLieu") String linkLieu,
            @RequestParam("date") String date,
            @RequestParam("startTime") String startTime,
            @RequestParam("endTime") String endTime,
            @RequestParam(value="departementId", required = false) Integer departementId,
            @RequestParam(value = "typeEventId", required = false) Integer typeEventId,
            @RequestParam("image") MultipartFile file) throws IOException {

        Departement departement = departementRepository.findById(departementId)
                .orElseThrow(() -> new RuntimeException("Departement not found"));

        TypeEvent eventType = typeEventRepository.findById(typeEventId)
                .orElseThrow(() -> new RuntimeException("Type Event non trouvé"));

        Event event = new Event();
        event.setTitle(title);
        event.setDescription(description);
        event.setLieu(lieu);
        event.setLinkLieu(linkLieu);
        event.setDate(date);
        event.setStartTime(startTime);
        event.setEndTime(endTime);
        event.setDepartement(departement);
        event.setTypeEvent(eventType);

        Event createdEvent = eventService.saveEvent(event, file);
        return ResponseEntity.ok(createdEvent);
    }

    @Operation(summary = "Update an existing event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Event updated successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Event.class)) }),
            @ApiResponse(responseCode = "404", description = "Event not found",
                    content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity<Event> updateEvent( @PathVariable Long id,
                                              @RequestParam(value = "title", required = false) String title,
                                              @RequestParam(value = "description", required = false) String description,
                                              @RequestParam(value = "lieu", required = false) String lieu,
                                              @RequestParam(value = "date", required = false) String date,
                                              @RequestParam(value = "startTime", required = false) String startTime,
                                              @RequestParam(value = "endTime", required = false) String endTime,
                                              @RequestParam(value = "departementId", required = false) Integer departementId,
                                              @RequestParam(value = "typeEventId", required = false) Integer typeEventId,
                                              @RequestParam(value = "trainerId", required = false) Long trainerId,
                                              @RequestParam(value = "image", required = false) MultipartFile file) throws IOException {

        Event eventToUpdate = eventRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Événement non trouvé"));

        if (title != null) eventToUpdate.setTitle(title);
        if (description != null) eventToUpdate.setDescription(description);
        if (lieu != null) eventToUpdate.setLieu(lieu);
        if (date != null) eventToUpdate.setDate(date);
        if (startTime != null) eventToUpdate.setStartTime(startTime);
        if (endTime != null) eventToUpdate.setEndTime(endTime);

        if (departementId != null) {
            Departement departement = departementRepository.findById(departementId)
                    .orElseThrow(() -> new RuntimeException("Département non trouvé"));
            eventToUpdate.setDepartement(departement);
        }

        if (typeEventId != null) {
            TypeEvent eventType = typeEventRepository.findById(typeEventId)
                    .orElseThrow(() -> new RuntimeException("Type Event non trouvé"));
            eventToUpdate.setTypeEvent(eventType);
        }

        if (trainerId != null) {
            Formateur formateur = formateurRepository.findById(trainerId)
                    .orElseThrow(() -> new RuntimeException("Trainer non trouvé"));
            eventToUpdate.setFormateur(formateur);
        }

        if (file != null && !file.isEmpty()) {
            eventService.saveEvent(eventToUpdate, file); // Assurez-vous que cette méthode gère correctement le fichier
        } else {
            eventRepository.save(eventToUpdate);
        }

        return ResponseEntity.ok(eventToUpdate);
    }

    @Operation(summary = "Add a trainer to an event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Trainer added to event successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Event.class)) }),
            @ApiResponse(responseCode = "404", description = "Event or Trainer not found",
                    content = @Content)
    })
    @PutMapping("/{eventId}/formateurs/{formateurId}")
    public ResponseEntity<Event> addFormateurToEvent(
            @PathVariable Long eventId,
            @PathVariable Long formateurId) {
        Event event = eventRepository.findById(eventId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Événement non trouvé"));
        Formateur formateur = formateurRepository.findById(formateurId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Formateur non trouvé"));

        event.setFormateur(formateur);
        eventRepository.save(event);
        return ResponseEntity.ok(event);
    }

    @Operation(summary = "Delete an event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Event deleted successfully",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Event not found",
                    content = @Content)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEvent(@PathVariable Long id) {
        eventService.deleteEvent(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Get total number of events")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Total number of events",
                    content = { @Content(mediaType = "application/json") })
    })
    @GetMapping("/total")
    public long getTotalEvents() {
        return eventService.getTotalNumberOfEvents();
    }
}
