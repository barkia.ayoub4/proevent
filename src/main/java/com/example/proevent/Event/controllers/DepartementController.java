package com.example.proevent.Event.controllers;

import com.example.proevent.Event.entities.Departement;
import com.example.proevent.Event.services.DepartementService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/departements")
public class DepartementController {

    private final DepartementService departementService;

    public DepartementController(DepartementService departementService) {
        this.departementService = departementService;
    }

    @Operation(summary = "Get all departements")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of all departements",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Departement.class)) })
    })
    @GetMapping()
    public List<Departement> departementList() {
        return departementService.getAllDepartements();
    }

    @Operation(summary = "Get a departement by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Departement found",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Departement.class)) }),
            @ApiResponse(responseCode = "404", description = "Departement not found",
                    content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<Departement> getDepartementById(@PathVariable Integer id) {
        Departement departement = departementService.getDepartementById(id).orElseThrow();
        return new ResponseEntity<>(departement, HttpStatus.OK);
    }

    @Operation(summary = "Create a new departement")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Departement created successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Departement.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid input",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity<Departement> create(@Valid @RequestBody Departement departement) {
        Departement createdDepartement = departementService.saveDepartement(departement);
        return new ResponseEntity<>(createdDepartement, HttpStatus.CREATED);
    }

    @Operation(summary = "Update an existing departement")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Departement updated successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Departement.class)) }),
            @ApiResponse(responseCode = "404", description = "Departement not found",
                    content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity<Departement> update(@Valid @RequestBody Departement newDepartement, @PathVariable Integer id) {
        Departement updatedDepartement = departementService.updateDepartement(newDepartement, id);
        return new ResponseEntity<>(updatedDepartement, HttpStatus.OK);
    }

    @Operation(summary = "Delete a departement")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Departement deleted successfully",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Departement not found",
                    content = @Content)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        departementService.deleteDepartement(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Get event count by department")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Event count by department",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Map.class)) })
    })
    @GetMapping("/stats")
    public ResponseEntity<Map<String, Integer>> getEventCountByDepartment() {
        Map<String, Integer> stats = departementService.getEventCountByDepartment();
        return new ResponseEntity<>(stats, HttpStatus.OK);
    }

}