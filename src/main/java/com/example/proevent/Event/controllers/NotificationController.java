package com.example.proevent.Event.controllers;

import com.example.proevent.Event.entities.NotificationRequest;
import com.example.proevent.Event.entities.NotificationResponse;
import com.example.proevent.Event.services.FCMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
@RequestMapping("/api/notification")

@RestController
public class NotificationController {
    @Autowired
    private FCMService fcmService;

    @PostMapping()
    public ResponseEntity sendNotification(@RequestBody NotificationRequest request) throws ExecutionException, InterruptedException {
        fcmService.sendMessageToToken(request);
        return new ResponseEntity<>(new NotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }

}