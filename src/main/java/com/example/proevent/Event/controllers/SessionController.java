package com.example.proevent.Event.controllers;

import com.example.proevent.Event.entities.Event;
import com.example.proevent.Event.entities.Formateur;
import com.example.proevent.Event.entities.Session;
import com.example.proevent.Event.repositories.EventRepository;
import com.example.proevent.Event.repositories.FormateurRepository;
import com.example.proevent.Event.repositories.SessionRepository;
import com.example.proevent.Event.services.SessionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/sessions")
public class SessionController {

    private final SessionService sessionService;
    private final EventRepository eventRepository;
    private final SessionRepository sessionRepository;
    private final FormateurRepository formateurRepository;

    public SessionController(SessionService sessionService, EventRepository eventRepository, SessionRepository sessionRepository, FormateurRepository formateurRepository) {
        this.sessionService = sessionService;
        this.eventRepository = eventRepository;
        this.sessionRepository = sessionRepository;
        this.formateurRepository = formateurRepository;
    }

    @Operation(summary = "Get list of all sessions")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of sessions retrieved successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Session.class)) })
    })
    @GetMapping()
    public List<Session> sessionList() {
        return sessionService.getAllSessions();
    }

    @Operation(summary = "Get a session by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Session retrieved successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Session.class)) }),
            @ApiResponse(responseCode = "404", description = "Session not found", content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<Session> getSessionById(@PathVariable Long id) {
        Session session = sessionService.getSessionById(id);
        return new ResponseEntity<>(session, HttpStatus.OK);
    }

    @Operation(summary = "Get sessions by event ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Sessions retrieved successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Session.class)) })
    })
    @GetMapping("/EventSession/{eventId}")
    public List<Session> getSessionsByEventId(@PathVariable Long eventId) {
        return sessionRepository.findByEventId(eventId);
    }

    @Operation(summary = "Create a new session")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Session created successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Session.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid input", content = @Content)
    })
    @PostMapping(consumes = {"multipart/form-data"})
    public ResponseEntity<Session> createSession(@RequestPart("session") @Valid String sessionJson,
                                                 @RequestParam("eventId") Long eventId) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Session session = mapper.readValue(sessionJson, Session.class);
        Event event = eventRepository.findById(eventId).orElseThrow(()
                -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found"));
        session.setEvent(event);
        Session createdSession = sessionService.saveSession(session);
        return new ResponseEntity<>(createdSession, HttpStatus.CREATED);
    }

    @Operation(summary = "Update a session")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Session updated successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Session.class)) }),
            @ApiResponse(responseCode = "404", description = "Session not found", content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity<Session> update(@Valid @RequestBody Session newSession, @PathVariable Long id) {
        Session updatedSession = sessionService.updateSession(newSession, id);
        return new ResponseEntity<>(updatedSession, HttpStatus.OK);
    }

    @Operation(summary = "Add a formateur to a session")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Formateur added to session successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Session.class)) }),
            @ApiResponse(responseCode = "404", description = "Session or formateur not found", content = @Content)
    })
    @PutMapping("/{sessionId}/formateurs/{formateurId}")
    public ResponseEntity<Session> addFormateurToSession(
            @PathVariable Long sessionId,
            @PathVariable Long formateurId) {
        Session session = sessionRepository.findById(sessionId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Session not found"));
        Formateur formateur = formateurRepository.findById(formateurId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Formateur not found"));

        session.setFormateur(formateur);
        sessionRepository.save(session);
        return ResponseEntity.ok(session);
    }

    @Operation(summary = "Delete a session")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Session deleted successfully", content = @Content),
            @ApiResponse(responseCode = "404", description = "Session not found", content = @Content)
    })
    @DeleteMapping("/{sessionId}")
    public void deleteSession(@PathVariable Long sessionId) {
        Session session = sessionRepository.findById(sessionId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Session not found"));
        sessionRepository.delete(session);
    }
}
