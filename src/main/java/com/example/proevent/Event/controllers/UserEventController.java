package com.example.proevent.Event.controllers;

import com.example.proevent.Event.entities.Event;
import com.example.proevent.Event.entities.UserEvent;
import com.example.proevent.Event.repositories.EventRepository;
import com.example.proevent.Event.repositories.UserEventRepository;
import com.example.proevent.Event.services.UserEventService;
import com.example.proevent.authentification.entities.User;
import com.example.proevent.authentification.repositries.UserRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/userevent")
public class UserEventController {

    private final UserEventService userEventService;
    private final UserRepository userRepository;
    private final EventRepository eventRepository;
    private final UserEventRepository userEventRepository;

    @Autowired
    public UserEventController(UserEventService userEventService, UserRepository userRepository, EventRepository eventRepository, UserEventRepository userEventRepository) {
        this.userEventService = userEventService;
        this.userRepository = userRepository;
        this.eventRepository = eventRepository;
        this.userEventRepository = userEventRepository;
    }

    @Operation(summary = "Add a user to an event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User added to event successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserEvent.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content),
            @ApiResponse(responseCode = "404", description = "User or event not found", content = @Content)
    })
    @PostMapping("/add/{eventId}/users/{userId}")
    public ResponseEntity<UserEvent> addUserEvent(@PathVariable Long eventId,
                                                  @PathVariable Integer userId,
                                                  @RequestBody UserEvent userEvent) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User non trouvé"));
        Event event = eventRepository.findById(eventId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Événement non trouvé"));

        userEvent.setUser(user);
        userEvent.setEvent(event);

        try {
            UserEvent newUserEvent = userEventService.saveUserEventIfNotExists(userEvent);
            return ResponseEntity.status(HttpStatus.CREATED).body(newUserEvent);
        } catch (IllegalStateException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null); // Adjust the body or status as needed
        }
    }

    @Operation(summary = "Get all user events")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of all user events",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserEvent.class)) })
    })
    @GetMapping("/all")
    public ResponseEntity<List<UserEvent>> getAllUserEvents() {
        List<UserEvent> userEvents = userEventService.getAllUserEvents();
        return new ResponseEntity<>(userEvents, HttpStatus.OK);
    }

    @Operation(summary = "Get user events by event ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User events found for the event ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserEvent.class)) }),
            @ApiResponse(responseCode = "404", description = "User events not found for the event ID", content = @Content)
    })
    @GetMapping("/events/{eventId}")
    public ResponseEntity<List<UserEvent>> getUserEventsByEventId(@PathVariable Long eventId) {
        List<UserEvent> userEvents = userEventService.getUserEventsByEventId(eventId);
        if (userEvents.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(userEvents, HttpStatus.OK);
    }

    @Operation(summary = "Get events by user ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Events found for the user ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserEvent.class)) }),
            @ApiResponse(responseCode = "404", description = "Events not found for the user ID", content = @Content)
    })
    @GetMapping("/user/{userId}/events")
    public ResponseEntity<List<UserEvent>> getEventsByUserId(@PathVariable Long userId) {
        List<UserEvent> userEvents = userEventRepository.findByUserId(userId);
        if (userEvents.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(userEvents, HttpStatus.OK);
    }

    @Operation(summary = "Delete a user event association")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "User event association deleted successfully", content = @Content)
    })
    @DeleteMapping("/delete/{eventId}/{userId}")
    public ResponseEntity<?> deleteUserEvent(@PathVariable Long eventId, @PathVariable Long userId) {
        userEventRepository.deleteByEventIdAndUserId(eventId, userId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Update a user event association")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User event association updated successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserEvent.class)) }),
            @ApiResponse(responseCode = "404", description = "User event association not found", content = @Content)
    })
    @PutMapping("/update/{eventId}/users/{userId}")
    public ResponseEntity<UserEvent> updateUserEvent(@PathVariable Long eventId,
                                                     @PathVariable Integer userId,
                                                     @RequestBody UserEvent userEventDetails) {

        // Retrieve the existing UserEvent
        UserEvent existingUserEvent = userEventRepository.findByUserIdAndEventId(userId, eventId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Association User-Événement non trouvé"));

        // Update the fields of the existing UserEvent
        if (userEventDetails.getRate() != null) existingUserEvent.setRate(userEventDetails.getRate());
        if (userEventDetails.isApproved()) existingUserEvent.setApproved(userEventDetails.isApproved());

        // Update the UserEvent
        UserEvent updatedUserEvent = userEventService.updateUserEvent(existingUserEvent);
        return new ResponseEntity<>(updatedUserEvent, HttpStatus.OK);
    }

    @Operation(summary = "Get satisfaction counts by event ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Satisfaction counts retrieved successfully",
                    content = { @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", description = "Satisfaction counts not found", content = @Content)
    })
    @GetMapping("/satisfaction/{eventId}")
    public ResponseEntity<Map<String, Integer>> getSatisfactionCountsByEventId(@PathVariable Long eventId) {
        Map<String, Integer> satisfactionCounts = userEventService.countUserSatisfactionByEventId(eventId);
        if (satisfactionCounts.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(satisfactionCounts, HttpStatus.OK);
    }
}
