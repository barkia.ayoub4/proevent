package com.example.proevent.Event.controllers;

import com.example.proevent.Event.entities.Partenaire;
import com.example.proevent.Event.services.PartenaireService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/api/partenaires")
public class PartenaireController {
    public static String uploadDirectory =System.getProperty("user.dir") + "src/main/webapp/images";

    private final PartenaireService partenaireService;

    public PartenaireController(PartenaireService partenaireService) {
        this.partenaireService = partenaireService;
    }

    @GetMapping()
    public List<Partenaire> partenaireList() {
        return partenaireService.getAllPartenaires();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Partenaire> getPartenaireById(@PathVariable Long id) {
        Partenaire partenaire = partenaireService.getPartenaireById(id);
        return new ResponseEntity<>(partenaire, HttpStatus.OK);
    }

    @PostMapping()
    public RedirectView saveUser(Partenaire partenaire,
                                 @RequestParam("image") MultipartFile multipartFile) throws IOException {

        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        partenaire.setProfileImage(fileName);
        Partenaire savePartenaire = partenaireService.savePartenaire(partenaire);

        String uploadDir = "partenaire-photo/" + savePartenaire.getId();

        FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);

        return new RedirectView("/partenaires", true);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Partenaire> update(@Valid @RequestBody Partenaire newPartenaire, @PathVariable Long id) {
        Partenaire updatedPartenaire = partenaireService.updatePartenaire(newPartenaire, id);
        return new ResponseEntity<>(updatedPartenaire, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePartenaire(@PathVariable Long id) {
        partenaireService.deletePartenaire(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
