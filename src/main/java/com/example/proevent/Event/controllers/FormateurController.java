package com.example.proevent.Event.controllers;

import com.example.proevent.Event.entities.Formateur;
import com.example.proevent.Event.repositories.FormateurRepository;
import com.example.proevent.Event.repositories.SessionRepository;
import com.example.proevent.Event.services.FormateurService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/formateurs")
public class FormateurController {

    private final FormateurService formateurService;
    private final FormateurRepository formateurRepository;
    private final SessionRepository sessionRepository;

    public FormateurController(FormateurService formateurService, FormateurRepository formateurRepository, SessionRepository sessionRepository) {
        this.formateurService = formateurService;
        this.formateurRepository = formateurRepository;
        this.sessionRepository = sessionRepository;
    }

    @Operation(summary = "Get list of all formateurs")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of formateurs retrieved successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Formateur.class)) })
    })
    @GetMapping()
    public List<Formateur> formateurList() {
        return formateurService.getAllFormateurs();
    }

    @Operation(summary = "Get a formateur by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Formateur retrieved successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Formateur.class)) }),
            @ApiResponse(responseCode = "404", description = "Formateur not found", content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<Formateur> getFormateurById(@PathVariable Long id) {
        Formateur formateur = formateurService.getFormateurById(id);
        return new ResponseEntity<>(formateur, HttpStatus.OK);
    }

    @Operation(summary = "Create a new formateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Formateur created successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Formateur.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid input", content = @Content)
    })
    @PostMapping
    public ResponseEntity<Formateur> createFormateur(
            @RequestParam("name") String name,
            @RequestParam("email") String email,
            @RequestParam(value = "image", required = false) MultipartFile file
    ) throws IOException {
        Formateur formateur = new Formateur();
        formateur.setName(name);
        formateur.setEmail(email);

        if (file != null && !file.isEmpty()) {
            formateurService.saveFormateur(formateur, file);
        } else {
            formateurRepository.save(formateur);
        }

        return new ResponseEntity<>(formateur, HttpStatus.CREATED);
    }

    @Operation(summary = "Update a formateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Formateur updated successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Formateur.class)) }),
            @ApiResponse(responseCode = "404", description = "Formateur not found", content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity<Formateur> update(
            @PathVariable Long id,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "image", required = false) MultipartFile file) {
        try {
            Formateur formateurToUpdate = formateurService.getFormateurById(id);
            if (formateurToUpdate == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            if (name != null) formateurToUpdate.setName(name);
            if (email != null) formateurToUpdate.setEmail(email);
            if (file != null && !file.isEmpty()) {
                formateurService.saveFormateur(formateurToUpdate, file);
            }

            Formateur updatedFormateur = formateurService.updateFormateur(formateurToUpdate, id);
            return new ResponseEntity<>(updatedFormateur, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Delete a formateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Formateur deleted successfully", content = @Content),
            @ApiResponse(responseCode = "404", description = "Formateur not found", content = @Content)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteFormateur(@PathVariable Long id) {
        formateurService.deleteFormateur(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
