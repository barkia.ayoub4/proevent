package com.example.proevent.Dto;

import com.example.proevent.authentification.entities.Role;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

public class UserDto {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Role role;
    private Integer departementId;

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    private boolean valid;
    // private byte[] profilImage;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    public Integer getDepartementId() {
        return departementId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    public void setDepartementId(Integer departementId) {
        this.departementId = departementId;
    }

}
