package com.example.proevent.authentification.repositries;

import com.example.proevent.authentification.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Integer> {
    Optional<User> findByEmail(String email) ;

    @Query("select u from User u join u.departement d where d.id = :departementId")
    List<User> findAllUsersByDepartementId(Long departementId);

    boolean existsByEmail(String email);

    @Query("SELECT COUNT(u) FROM User u WHERE u.valid = true")
    long countAllUsers();

}
