package com.example.proevent.authentification.services;

import com.example.proevent.authentification.entities.Role;
import com.example.proevent.authentification.entities.User;
import com.example.proevent.authentification.jwt.AuthentificationRequest;
import com.example.proevent.authentification.jwt.AuthentificationResponse;
import com.example.proevent.authentification.jwt.ChangePasswordRequest;
import com.example.proevent.authentification.jwt.RegisterRequest;
import com.example.proevent.authentification.Token.TokenRepository;
import com.example.proevent.authentification.repositries.UserRepository;
import com.example.proevent.authentification.Token.Token;
import com.example.proevent.authentification.Token.TokenType;
import com.example.proevent.authentification.utils.EmailUtil;
import com.example.proevent.authentification.utils.OtpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.mail.MessagingException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor

public class AuthServiceImpl implements AuthService{

    private final UserRepository userRepository ;
    private final TokenRepository tokenRepository;
    private final PasswordEncoder passwordEncoder ;
    private final JwtService jwtService ;
    private final AuthenticationManager authenticationManager ;
    private final OtpUtil otpUtil;
    private final EmailUtil emailUtil;

    @Override
    public void addRoleToUser(String username, String rolename) {

    }

    public AuthentificationResponse register(RegisterRequest request) {

        String otp =otpUtil.generateOtp();
        try {
            emailUtil.sendOtpEmail(request.getEmail(),otp);
        } catch (MessagingException e) {
            throw new RuntimeException("Unable to send otp please try again");
        }

        var user= User.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.USER)
                .otp(otp)
                .otpGeneratedTime(LocalDateTime.now())
                .build();

        var savedUser=userRepository.save(user);
        var jwtToken = jwtService.generateToken(user);

        savedUserToken(savedUser,jwtToken);
        var refreshToken=jwtService.generateRefreshToken(user);
        var userId=user.getId();
        return AuthentificationResponse.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .userId(userId)
                .build();
    }



    public Object authenticate (AuthentificationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        var user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new RuntimeException("User not found with this email: " +request.getEmail()));
        var jwtToken = jwtService.generateToken(user);
        revokeAllUserTokens(user);
        savedUserToken(user,jwtToken);
        var refreshToken=jwtService.generateRefreshToken(user);
        var userId=user.getId();
        if (!user.isActive()) {
            String jsonResponse;
            jsonResponse = "{\"status\": \"error\", \"message\": \"Your account is not active. Please contact support.\"}";
            return jsonResponse;
        } else if (!user.isValid()) {
            String jsonResponse;
            jsonResponse = "{\"status\": \"error\", \"message\": \"Your account is not approved. Please contact support.\"}";
            return jsonResponse;
        } else {
            AuthentificationResponse jsonResponse;
            jsonResponse=AuthentificationResponse.builder()
                    .accessToken(jwtToken)
                    .refreshToken(refreshToken)
                    .userId(userId)
                    .build();
        return  jsonResponse ;
        }
    }



    public String validAccount(String email, String otp) {
        String jsonResponse;
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new RuntimeException("User not found with this email: " + email));
        if (user.getOtp().equals(otp) && Duration.between(user.getOtpGeneratedTime(),
                LocalDateTime.now()).getSeconds() < (1 * 60)) {
            user.setActive(true);
            userRepository.save(user);
            jsonResponse = "{\"status\": \"success\", \"message\": \"OTP verified, you can login\"}";
        } else {
            jsonResponse = "{\"status\": \"error\", \"message\": \"Please regenerate OTP and try again\"}";
        }
        return  jsonResponse ;
    }


    public String regenerateOtp(String email) {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new RuntimeException("User not found with this email: " + email));
        String otp = otpUtil.generateOtp();
        String jsonResponse;

        try {
            emailUtil.sendOtpEmail(email, otp);
        } catch (MessagingException e) {
            throw new RuntimeException("Unable to send otp please try again");
        }
        user.setOtp(otp);
        user.setOtpGeneratedTime(LocalDateTime.now());
        userRepository.save(user);
        jsonResponse = "{\"status\": \"success\", \"message\": \"Email sent... please verify account within 1 minute\"}";

        return jsonResponse;
    }


    public String forgotPassword(String email) {
        var user = userRepository.findByEmail(email)
                .orElseThrow(() -> new RuntimeException("User not found with this email: " + email));
        String otp = otpUtil.generateOtp();
        user.setOtp(otp);
        userRepository.save(user);
        String jsonResponse;
        try {
            emailUtil.sendSetPasswordEmail(email, otp);
        } catch (MessagingException e) {
            throw new RuntimeException("Unable to send otp please try again");
        }
        jsonResponse = "{\"status\": \"success\", \"message\": \"Email sent... please verify account within 1 minute\"}";

        return jsonResponse;
    }

    public void changePassword(String email,ChangePasswordRequest request) {

        var user = userRepository.findByEmail(email)
                .orElseThrow(() -> new RuntimeException("User not found with this email: " + email));

        // check if the two new passwords are the same
        if (!request.getNewPassword().equals(request.getConfirmationPassword())) {
            throw new IllegalStateException("Password are not the same");
        }
        // update the password
        user.setPassword(passwordEncoder.encode(request.getNewPassword()));

        // save the new password
        userRepository.save(user);
    }


    public String setOtp(String email, String otp) {

        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new RuntimeException("User not found with this email: " + email));

        String jsonResponse;
        if (user.getOtp().equals(otp)) {
            jsonResponse = "{\"status\": \"success\", \"message\": \"Otp verified... please login\"}";
        }  else {
            jsonResponse = "{\"status\": \"error\", \"message\": \"Otp is wrong\"}";

        }
        return jsonResponse ;
    }

    @Override
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            final String refreshToken = authorizationHeader.substring(7);
            final String email = jwtService.extractUsername(refreshToken);

            if (email != null) {
                var user = userRepository.findByEmail(email)
                        .orElseThrow(() -> new RuntimeException("User not found with this email: " + email));

                if (jwtService.isTokenValid(refreshToken, user)) {
                    var newAccessToken = jwtService.generateToken(user);
                    var authResponse = AuthentificationResponse.builder()
                            .accessToken(newAccessToken)
                            .refreshToken(refreshToken)
                            .build();
                    response.setContentType("application/json");
                    new ObjectMapper().writeValue(response.getOutputStream(), authResponse);
                    return; // Assurez-vous de retourner après avoir écrit la réponse
                }
            }
        }

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); // Réponse en cas de token non valide
    }

    private void savedUserToken(User user, String jwtToken) {
        Token existingToken = tokenRepository.findByTokenAndUser(jwtToken, user).orElse(null);
        if (existingToken != null) {
             existingToken.setExpired(false);
            tokenRepository.save(existingToken);
        } else {
            Token token = Token.builder()
                    .user(user)
                    .token(jwtToken)
                    .tokenType(TokenType.BEARER)
                    .expired(false)
                    .revoked(false)
                    .build();
            tokenRepository.save(token);
        }
    }

    private void revokeAllUserTokens(User user) {
        var validUserTokens = tokenRepository.findAllValidTokensByUser(user.getId());
        if (validUserTokens.isEmpty())
            return;
        validUserTokens.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens);
    }


}


