package com.example.proevent.authentification.services;

import com.example.proevent.authentification.jwt.AuthentificationRequest;
import com.example.proevent.authentification.jwt.AuthentificationResponse;
import com.example.proevent.authentification.jwt.ChangePasswordRequest;
import com.example.proevent.authentification.jwt.RegisterRequest;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;


public interface AuthService {
    void addRoleToUser(String username,String rolename);
   AuthentificationResponse register(RegisterRequest request);
   Object authenticate (AuthentificationRequest request);
    void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException;

    String validAccount(String email, String otp);

    String regenerateOtp(String email);
    void changePassword(String email,ChangePasswordRequest request);

    String forgotPassword(String email);

    String setOtp(String email, String otp);

}
