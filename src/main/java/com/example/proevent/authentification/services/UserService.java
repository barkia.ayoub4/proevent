package com.example.proevent.authentification.services;

import com.example.proevent.authentification.Token.TokenRepository;
import com.example.proevent.authentification.entities.User;
import com.example.proevent.authentification.repositries.UserRepository;
import com.example.proevent.authentification.utils.EmailUtil;
import jakarta.mail.MessagingException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;
@Service
@RequiredArgsConstructor

public class UserService {

    private final UserRepository userRepository ;
    private final PasswordEncoder passwordEncoder;
    private final EmailUtil emailUtil;
    private final TokenRepository tokenRepository;
    public List<User> getAllUser (){
        return userRepository.findAll() ;
    }



    public User saveUser(User user )  {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }


    public User update(User newUser, Integer id) {
        if (!userRepository.existsById(id)) {throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");}
        User existingUser = userRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
        existingUser.updateUser(newUser);
        return userRepository.save(existingUser);
    }

    @Transactional
    public void deleteUser(int userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException("Utilisateur non trouvé"));
        tokenRepository.deleteByUser(user);
        userRepository.delete(user);
    }

    public User approveAccount(String email){
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new RuntimeException("User not found with this email: " + email));
        user.setValid(true);
        try {
            emailUtil.sendApproveAccountEmail(email);
        } catch (MessagingException e) {
            throw new RuntimeException("Unable to send otp please try again");
        }
        return userRepository.save(user);
    }
    public long getTotalNumberOfUsers() {
        return userRepository.countAllUsers();
    }
}
