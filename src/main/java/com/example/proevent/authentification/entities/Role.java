package com.example.proevent.authentification.entities;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor

public enum Role {
    USER,
    ADMIN

}
