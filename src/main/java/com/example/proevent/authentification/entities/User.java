package com.example.proevent.authentification.entities;

import com.example.proevent.Event.entities.Departement;
import com.example.proevent.Event.entities.Event;
import com.example.proevent.Event.entities.UserEvent;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.Email;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User implements UserDetails {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id ;

    private  String firstName ;
    private  String lastName ;

    @Column(unique = true)
    @Email
    private  String email ;
    private String password;

    @Enumerated(EnumType.STRING)
    private  Role role ;

    @Column(updatable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<UserEvent> events;


    @ManyToOne
    @JoinColumn(name = "departement_id")
    private Departement departement;


        private String image;

    private boolean active;
    private String otp;
    private LocalDateTime otpGeneratedTime;
    private boolean valid;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void updateUser(User newUser){

        if (newUser.getFirstName() != null) {
            this.setFirstName(newUser.getFirstName());
        }
        if (newUser.getLastName() != null) {
            this.setLastName(newUser.getLastName());
        }
        if (newUser.getEmail() != null) {
            this.setEmail(newUser.getEmail());
        }
        if (newUser.getRole() != null) {
            this.setRole(newUser.getRole());
        }
        if (newUser.getDepartement() != null) {
            this.setDepartement(newUser.getDepartement());
        }
        if (newUser.getImage() != null) {
            this.setImage(newUser.getImage());
        }
    }
}
