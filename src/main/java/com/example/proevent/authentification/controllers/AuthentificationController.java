package com.example.proevent.authentification.controllers;

import com.example.proevent.authentification.jwt.AuthentificationRequest;
import com.example.proevent.authentification.jwt.AuthentificationResponse;
import com.example.proevent.authentification.jwt.ChangePasswordRequest;
import com.example.proevent.authentification.jwt.RegisterRequest;
import com.example.proevent.authentification.repositries.UserRepository;
import com.example.proevent.authentification.services.AuthService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import java.io.IOException;

@RestController
@RequestMapping("api/auth")
public class AuthentificationController {
    @Autowired
    AuthService authService;
    @Autowired
    UserRepository userRepository;

    @Operation(summary = "Register a new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User registered successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AuthentificationResponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid input or email already in use",
                    content = @Content)
    })
    @PostMapping("/register")
    public ResponseEntity<AuthentificationResponse> register(
            @RequestBody RegisterRequest request
    ) {
        return ResponseEntity.ok(authService.register(request));
    }

    @Operation(summary = "Authenticate a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User authenticated successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Object.class)) }),
            @ApiResponse(responseCode = "401", description = "Invalid credentials",
                    content = @Content)
    })
    @PostMapping("/authentication")
    public ResponseEntity<Object> authentication(
            @RequestBody AuthentificationRequest request
    ) {
        return ResponseEntity.ok(authService.authenticate(request));
    }

    @Operation(summary = "Verify a user account with OTP")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Account verified successfully",
                    content = { @Content(mediaType = "text/plain",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid email or OTP",
                    content = @Content)
    })
    @PutMapping("/valid-account")
    public ResponseEntity<String> verifyAccount(@RequestParam String email,
                                                @RequestParam String otp) {
        return new ResponseEntity<>(authService.validAccount(email, otp), HttpStatus.OK);
    }

    @Operation(summary = "Regenerate OTP for account verification")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OTP regenerated successfully",
                    content = { @Content(mediaType = "text/plain",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid email",
                    content = @Content)
    })
    @PutMapping("/regenerate-otp")
    public ResponseEntity<String> regenerateOtp(@RequestParam String email) {
        return new ResponseEntity<>(authService.regenerateOtp(email), HttpStatus.OK);
    }

    @Operation(summary = "Initiate forgot password process")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Forgot password process initiated",
                    content = { @Content(mediaType = "text/plain",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid email",
                    content = @Content)
    })
    @PutMapping("/forgot-password")
    public ResponseEntity<String> forgotPassword(
            @RequestParam String email
    ) {
        return new ResponseEntity<>(authService.forgotPassword(email), HttpStatus.OK);
    }

    @Operation(summary = "Set OTP for password reset")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OTP set successfully",
                    content = { @Content(mediaType = "text/plain",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid email or OTP",
                    content = @Content)
    })
    @PutMapping("/set-otp")
    public ResponseEntity<String> setPassword(@RequestParam String email,
                                              @RequestParam String otp) {
        return new ResponseEntity<>(authService.setOtp(email, otp), HttpStatus.OK);
    }

    @Operation(summary = "Change password with OTP")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Password changed successfully",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid email or OTP",
                    content = @Content)
    })
    @PutMapping("/set-password")
    public ResponseEntity<?> changePassword(
            @RequestParam String email,
            @RequestBody ChangePasswordRequest request
    ) {
        authService.changePassword(email, request);
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Refresh authentication token")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Token refreshed successfully",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Invalid token",
                    content = @Content)
    })
    @PostMapping("/refresh-token")
    public void refreshToken(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException {
        authService.refreshToken(request, response);
    }
}
