package com.example.proevent.authentification.controllers;

import com.example.proevent.Dto.UserDto;
import com.example.proevent.Event.entities.Departement;
import com.example.proevent.Event.repositories.DepartementRepository;
import com.example.proevent.authentification.entities.User;
import com.example.proevent.authentification.repositries.UserRepository;
import com.example.proevent.authentification.services.UserService;
import com.example.proevent.utils.FileStorageService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/users")
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    DepartementRepository departementRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ObjectMapper objectMapper; // Ajout de l'ObjectMapper

    @Operation(summary = "Get all users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of users",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = User.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Users not found",
                    content = @Content) })
    @GetMapping
    public List<User> UserList() {
        return userRepository.findAll();
    }

    @Operation(summary = "Get a user by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User found",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = User.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid ID supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public User UserById(@PathVariable Integer id) {
        return userRepository.findById(id).orElse(null);
    }

    @Operation(summary = "Add a new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User created",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = User.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid input",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content) })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path="/addUser")
    public ResponseEntity<?> addUser(@Valid @RequestBody UserDto userDto) {
        try {
            Departement departement = departementRepository.findById(userDto.getDepartementId())
                    .orElseThrow(() -> new RuntimeException("Departement not found"));

            User user = User.builder()
                    .firstName(userDto.getFirstName())
                    .lastName(userDto.getLastName())
                    .email(userDto.getEmail())
                    .password(userDto.getPassword())
                    .role(userDto.getRole())
                    .departement(departement)
                    .build();

            User savedUser = userService.saveUser(user);
            return ResponseEntity.status(HttpStatus.CREATED).body(savedUser);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @Operation(summary = "Update a user by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User updated",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = User.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid input",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content) })
    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public ResponseEntity<?> update(
            @RequestPart(value = "userDto") @Valid String userDtoJson,
            @RequestPart(required = false) MultipartFile file,
            @PathVariable Integer id) {
        try {
            UserDto userDto = objectMapper.readValue(userDtoJson, UserDto.class);
            User existingUser = userRepository.findById(id)
                    .orElseThrow(() -> new RuntimeException("User not found"));

            Boolean newUserValid = userDto.isValid();
            boolean oldUserValid = existingUser.isValid();

            boolean validHasChanged = (newUserValid != null) && (newUserValid != oldUserValid);

            if (userDto.isValid()) {
                existingUser.setValid(newUserValid);
                if (validHasChanged && newUserValid) {
                    userService.approveAccount(existingUser.getEmail());
                }
            }

            if (userDto.getFirstName() != null) existingUser.setFirstName(userDto.getFirstName());
            if (userDto.getLastName() != null) existingUser.setLastName(userDto.getLastName());
            if (userDto.getEmail() != null) existingUser.setEmail(userDto.getEmail());
            if (userDto.getPassword() != null) existingUser.setPassword(passwordEncoder.encode(userDto.getPassword()));
            if (userDto.getRole() != null) existingUser.setRole(userDto.getRole());
            if (userDto.getDepartementId() != null) {
                Departement departement = departementRepository.findById(userDto.getDepartementId())
                        .orElseThrow(() -> new RuntimeException("Department not found"));
                existingUser.setDepartement(departement);
            }

            if (file != null && !file.isEmpty()) {
                String fileName = fileStorageService.storeFile(file, "profile");
                String imageUrl = "http://localhost:8080/profile/" + fileName;
                existingUser.setImage(imageUrl);
            }

            User updatedUser = userService.update(existingUser, id);
            return ResponseEntity.ok(updatedUser);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @Operation(summary = "Delete a user by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid ID supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content) })
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        userService.deleteUser(id);
    }

    @Operation(summary = "Get total number of users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Total number of users",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Long.class)) }) })
    @GetMapping("/total")
    public ResponseEntity<Long> getTotalUsers() {
        long totalUsers = userService.getTotalNumberOfUsers();
        return ResponseEntity.ok(totalUsers);
    }

}