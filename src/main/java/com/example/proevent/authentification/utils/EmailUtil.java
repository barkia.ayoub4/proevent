package com.example.proevent.authentification.utils;


import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;


@Component
public class EmailUtil {

    @Autowired
    private JavaMailSender javaMailSender;
    public void sendOtpEmail(String email, String otp) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
        mimeMessageHelper.setTo(email);
        mimeMessageHelper.setSubject("Vérification du code OTP");

        String htmlContent = """
                <!DOCTYPE html>
                <html>
                <head>
                    <title>Vérification du code OTP </title>
                </head>
                <body style="font-family: Arial, sans-serif; background-color: #f5f5f5; padding: 20px;">
                    <div style="background-color: #ffffff; border-radius: 5px; padding: 20px;">
                        <img src="http://10.0.2.2:8080/images/asm2.png" alt="ASM Logo">
                        <h2 style="color: #333333;">Vérification du code OTP 🚀</h2>
                        <p>Cher utilisateur ,</p>
                        <p>Votre code OTP est : <strong>%s</strong></p>
                        <p>Veuillez cliquer sur le lien ci-dessous pour vérifier votre compte :</p>
                        <a href="http://localhost:8080/api/auth/verify-account?email=%s&otp=%s" 
                            target="_blank" style="display: inline-block; padding: 10px 20px; background-color: #007bff; color: #ffffff; text-decoration: none; border-radius: 5px;">Vérifier le compte</a>
                       <p class="footer">Si vous n'avez pas demandé cette vérification, veuillez ignorer cet e-mail.</p>
                                           <p class="footer">Merci,<br>Votre équipe de support</p>
                                          <p class="thank-you">Merci de choisir <strong>ASM</strong>.</p>
                    </div>
                </body>
                </html>
                """.formatted(otp, email, otp);

        mimeMessageHelper.setText(htmlContent, true);

        javaMailSender.send(mimeMessage);
    }


    public void sendSetPasswordEmail(String email,String otp) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
        mimeMessageHelper.setTo(email);
        mimeMessageHelper.setSubject("Set Password");
        String htmlContent = """
                <!DOCTYPE html>
                <html>
                <head>
                    <title>Realisation du Password</title>
                </head>
                <body style="font-family: Arial, sans-serif; background-color: #f5f5f5; padding: 20px;">
                    <div style="background-color: #ffffff; border-radius: 5px; padding: 20px;">
                        <img src="src/main/resources/images/asm.png" alt="ASM Logo">
                        <h2 style="color: #333333;">Changement du Mot de passe</h2>
                        <p>Cher utilisateur,</p>
                        <p>Votre code OTP est : <strong>%s</strong></p>
                        <a href="http://localhost:8080/api/auth/set-password?email=%s&otp=%s" 
                            target="_blank" style="display: inline-block; padding: 10px 20px; background-color: #007bff; color: #ffffff; text-decoration: none; border-radius: 5px;">Vérifier le compte</a>
                        <p style="margin-top: 20px;">Si vous n'avez pas demandé cette vérification, veuillez ignorer cet e-mail.</p>
                        <p>Merci,</p>
                        <p>Votre équipe de support</p>
                    </div>
                </body>
                </html>
                """.formatted(otp, email, otp);

        mimeMessageHelper.setText(htmlContent, true);

        javaMailSender.send(mimeMessage);
    }



    public void sendApproveAccountEmail(String email) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
        mimeMessageHelper.setTo(email);
        mimeMessageHelper.setSubject("Account approved");
        String htmlContent = """
                <!DOCTYPE html>
                        <html>
                        <head>
                            <title>Confirmation d'approbation de compte</title>
                        </head>
                        <body style="font-family: Arial, sans-serif; background-color: #f5f5f5; padding: 20px;">
                            <div style="background-color: #ffffff; border-radius: 5px; padding: 20px;">
                                <img src="http://10.0.2.2:8080/images/asm2.png" alt="ASM Logo">
                                <h2 style="color: #333333;">Confirmation d'approbation de compte 🚀</h2>
                                <p>Cher employé ,</p>
                                <p>Votre compte a été approuvé par l'administrateur.</p>
                                <p>Vous pouvez maintenant accéder à l'application pour consulter la liste des événements et y participer.</p>
                                <a target="_blank" style="display: inline-block; padding: 10px 20px; background-color: #007bff; color: #ffffff; text-decoration: none; border-radius: 5px;">Accéder à l'application</a>
                               <p class="footer">Si vous avez des questions, n'hésitez pas à nous contacter.</p>
                                                               <p class="footer">Merci,<br>Votre équipe de support</p>
                                                              <p class="thank-you">Merci de choisir <strong>ASM</strong>.</p>
                            </div>
                        </body>
                        </html>
                        
                """.formatted();

        mimeMessageHelper.setText(htmlContent, true);

        javaMailSender.send(mimeMessage);
    }

}

