package com.example.proevent.authentification.jwt;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ChangePasswordRequest {

    private String newPassword;
    private String confirmationPassword;
}